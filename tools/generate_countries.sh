#!/bin/bash

################################################################################
#
# Copyright (c) 2014  Haixing Hu
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################

THIS_DIR=$(dirname "$0");
CLDR_DIR=${THIS_DIR}/../cldr;
OUTPUT_DIR=${THIS_DIR}/../src/main/resources/country;
DEFAULT_LOC_BUNDLE="${OUTPUT_DIR}/Country_en.properties";
DEFAULT_BUNDLE="${OUTPUT_DIR}/Country.properties";
LICENSE_COMMENT="${THIS_DIR}/license_comment.txt";

for dir in $CLDR_DIR/*; do
	loc=${dir#$CLDR_DIR/};
	file="${dir}/territories.json";
	echo "Extracting country names for locale ${loc}";
	output_file="${OUTPUT_DIR}/Country_${loc//-/_}.properties";
	cat "${LICENSE_COMMENT}" > "${output_file}";
	jq ".main.\"${loc}\".localeDisplayNames.territories" $file \
		| sed -e '/[{}]/d' \
			  -e 's/^[[:space:]]*//g' \
			  -e 's/:/ =/g' \
			  -e 's/"//g' \
			  -e 's/,//g' \
			  -e '/[0-9]\{3\}/d' \
			  -e '/-alt-/d' \
		>> "${output_file}";
done
command cp -f "${DEFAULT_LOC_BUNDLE}" "${DEFAULT_BUNDLE}";

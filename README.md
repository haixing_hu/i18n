# i18n

The library providing internationalization utilities.

## Components

- `Country`: represents countries and territories.
- `Language`: repesents spoken languages.
- `Script`: represents written scripts.

All the above classes contain a function to get the display names for a specified locale.

## Data

The data of this library comes from the [CLDR (Unicode Common Locale Data Repository)](http://cldr.unicode.org).

The library also contains the scripts used to generate Java properties files from CLDR's json data.

## Dependent Projects

This project depends on the following projects. The depended projects **must** be built in the order listed below.

* [pom-root](https://bitbucket.org/haixing_hu/pom-root)
* [commons](https://bitbucket.org/haixing_hu/commons): which provides commonly used classes and functions for my personal Java programming.

## Build

1. Install and configure the JDK 8.0 or above.
2. Checks out the codes of this project and all its depended projects;
3. Build the depended projects in the order of above (**building order is important!**).
4. Build this project.
5. All the projects are managed by [maven](http://maven.apache.org/), so the building is as easy as typing `mvn clean install`.
/**
 * Copyright (c) 2014, Haixing Hu
 *
 * All rights reserved.
 */
package org.bitbucket.haixing_hu.i18n;

import java.util.Collection;
import java.util.Locale;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit test of the {@link Script} class.
 *
 * @author Haixing Hu
 */
public class ScriptTest {

  @Test
  public void testGetAll() {
    final Collection<Script> Scripts = Script.getAll();
    assertNotNull(Scripts);
    System.out.println("All registered scripts are: ");
    for (final Script script : Scripts) {
      System.out.println(script);
    }
  }

  @Test
  public void testGet() {
    final Script hans = Script.get("Hans");
    assertNotNull(hans);
    assertEquals("Hans", hans.getCode());
    assertEquals("Simplified", hans.getName());

    final Script jpan = Script.get("Jpan");
    assertNotNull(jpan);
    assertEquals("Jpan", jpan.getCode());
    assertEquals("Japanese", jpan.getName());
  }

  @Test
  public void testGetDisplayName() {
    final Locale en_US = new Locale("en_US");
    final Locale zh_CN = new Locale("zh_CN");

    final Script hans = Script.get("Hans");
    assertNotNull(hans);
    assertEquals("Simplified", hans.getDisplayName(en_US));
    assertEquals("简体中文", hans.getDisplayName(zh_CN));

    final Script jpan = Script.get("Jpan");
    assertNotNull(jpan);
    assertEquals("Japanese", jpan.getDisplayName(en_US));
    assertEquals("日文", jpan.getDisplayName(zh_CN));

    //System.out.println("Localized name for all scripts:");
    for (final Script script : Script.getAll()) {
      //System.out.println(script.getCode());
      for (final Locale locale : Locale.getAvailableLocales()) {
        final String name = script.getDisplayName(locale);
        assertNotNull(name);
//        System.out.printf("\t%s: %s\n", locale.toString(), name);
      }
    }
  }

  @Test
  public void testHashCode() {
    final Script hans1 = Script.get("Hans");
    final Script hans2 = Script.get("Hans");
    final Script japn1 = Script.get("Jpan");
    final Script japn2 = Script.get("Jpan");

    assertEquals(hans1.hashCode(), hans1.hashCode());
    assertEquals(hans1.hashCode(), hans2.hashCode());
    assertEquals(japn1.hashCode(), japn1.hashCode());
    assertEquals(japn1.hashCode(), japn2.hashCode());
    assertNotEquals(hans1.hashCode(), japn1.hashCode());
  }

  @Test
  public void testEquals() {
    final Script hans1 = Script.get("Hans");
    final Script hans2 = Script.get("Hans");
    final Script japn1 = Script.get("Jpan");
    final Script japn2 = Script.get("Jpan");

    assertEquals(true, hans1.equals(hans1));
    assertEquals(true, hans1.equals(hans2));
    assertEquals(true, japn1.equals(japn1));
    assertEquals(true, japn1.equals(japn2));
    assertEquals(false, hans1.equals(null));
    assertEquals(false, hans1.equals(japn1));
    assertEquals(false, hans1.equals("str"));
  }

  @Test
  public void testToString() {
    final Script hans = Script.get("Hans");
    System.out.println(hans.toString());

    final Script japn = Script.get("Jpan");
    System.out.println(japn.toString());
  }
}

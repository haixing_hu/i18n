/**
 * Copyright (c) 2014, Haixing Hu
 *
 * All rights reserved.
 */
package org.bitbucket.haixing_hu.i18n;

import java.util.Collection;
import java.util.Locale;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit test of the {@link Language} class.
 *
 * @author Haixing Hu
 */
public class LanguageTest {

  @Test
  public void testGetAll() {
    final Collection<Language> languages = Language.getAll();
    assertNotNull(languages);
    System.out.println("All registered languages are: ");
    for (final Language language : languages) {
      System.out.println(language);
    }
  }

  @Test
  public void testGet() {
    final Language en = Language.get("en");
    assertNotNull(en);
    assertEquals("en", en.getCode());
    assertEquals("English", en.getName());

    final Language zh = Language.get("zh");
    assertNotNull(zh);
    assertEquals("zh", zh.getCode());
    assertEquals("Chinese", zh.getName());
  }

  @Test
  public void testGetDisplayName() {
    final Locale en_US = new Locale("en_US");
    final Locale zh_CN = new Locale("zh_CN");

    final Language en = Language.get("en");
    assertNotNull(en);
    assertEquals("English", en.getDisplayName(en_US));
    assertEquals("英文", en.getDisplayName(zh_CN));

    final Language zh = Language.get("zh");
    assertNotNull(zh);
    assertEquals("Chinese", zh.getDisplayName(en_US));
    assertEquals("中文", zh.getDisplayName(zh_CN));

    //System.out.println("Localized name for all languages:");
    for (final Language language : Language.getAll()) {
      //System.out.println(language.getCode());
      for (final Locale locale : Locale.getAvailableLocales()) {
        final String name = language.getDisplayName(locale);
        assertNotNull(name);
//        System.out.printf("\t%s: %s\n", locale.toString(), name);
      }
    }
  }

  @Test
  public void testHashCode() {
    final Language en1 = Language.get("en");
    final Language en2 = Language.get("en");
    final Language zh1 = Language.get("zh");
    final Language zh2 = Language.get("zh");

    assertEquals(en1.hashCode(), en1.hashCode());
    assertEquals(en1.hashCode(), en2.hashCode());
    assertEquals(zh1.hashCode(), zh1.hashCode());
    assertEquals(zh1.hashCode(), zh2.hashCode());
    assertNotEquals(zh1.hashCode(), en1.hashCode());
  }

  @Test
  public void testEquals() {
    final Language en1 = Language.get("en");
    final Language en2 = Language.get("en");
    final Language zh1 = Language.get("zh");
    final Language zh2 = Language.get("zh");

    assertEquals(true, en1.equals(en1));
    assertEquals(true, en1.equals(en2));
    assertEquals(true, zh1.equals(zh1));
    assertEquals(true, zh1.equals(zh2));
    assertEquals(false, en1.equals(null));
    assertEquals(false, en1.equals(zh1));
    assertEquals(false, en1.equals("str"));
  }

  @Test
  public void testToString() {
    final Language en = Language.get("en");
    System.out.println(en.toString());

    final Language zh = Language.get("zh");
    System.out.println(zh.toString());
  }
}

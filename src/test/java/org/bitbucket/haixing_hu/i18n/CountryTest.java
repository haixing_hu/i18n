/**
 * Copyright (c) 2014, Haixing Hu
 *
 * All rights reserved.
 */
package org.bitbucket.haixing_hu.i18n;

import java.util.Collection;
import java.util.Locale;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Unit test of the {@link Country} class.
 *
 * @author Haixing Hu
 */
public class CountryTest {

  @Test
  public void testGetAll() {
    final Collection<Country> countries = Country.getAll();
    assertNotNull(countries);
    System.out.println("All registered countries are: ");
    for (final Country country : countries) {
      System.out.println(country);
    }
  }

  @Test
  public void testGet() {
    final Country us = Country.get("US");
    assertNotNull(us);
    assertEquals("US", us.getCode());
    assertEquals("United States", us.getName());

    final Country gb = Country.get("GB");
    assertNotNull(gb);
    assertEquals("GB", gb.getCode());
    assertEquals("United Kingdom", gb.getName());

    final Country cn = Country.get("CN");
    assertNotNull(cn);
    assertEquals("CN", cn.getCode());
    assertEquals("China", cn.getName());
  }

  @Test
  public void testGetDisplayName() {
    final Locale en_US = new Locale("en_US");
    final Locale zh_CN = new Locale("zh_CN");

    final Country us = Country.get("US");
    assertNotNull(us);
    assertEquals("United States", us.getDisplayName(en_US));
    assertEquals("美国", us.getDisplayName(zh_CN));

    final Country gb = Country.get("GB");
    assertNotNull(gb);
    assertEquals("United Kingdom", gb.getDisplayName(en_US));
    assertEquals("英国", gb.getDisplayName(zh_CN));

    final Country cn = Country.get("CN");
    assertNotNull(cn);
    assertEquals("China", cn.getDisplayName(en_US));
    assertEquals("中国", cn.getDisplayName(zh_CN));

    //System.out.println("Localized name for all countries:");
    for (final Country country : Country.getAll()) {
      //System.out.println(country.getCode());
      for (final Locale locale : Locale.getAvailableLocales()) {
        final String name = country.getDisplayName(locale);
        assertNotNull(name);
        //System.out.printf("\t%s: %s\n", locale.toString(), name);
      }
    }
  }

  @Test
  public void testHashCode() {
    final Country us1 = Country.get("US");
    final Country us2 = Country.get("US");
    final Country gb1 = Country.get("GB");
    final Country gb2 = Country.get("GB");

    assertEquals(us1.hashCode(), us1.hashCode());
    assertEquals(us1.hashCode(), us2.hashCode());
    assertEquals(gb1.hashCode(), gb1.hashCode());
    assertEquals(gb1.hashCode(), gb2.hashCode());
    assertNotEquals(us1.hashCode(), gb1.hashCode());
  }

  @Test
  public void testEquals() {
    final Country us1 = Country.get("US");
    final Country us2 = Country.get("US");
    final Country gb1 = Country.get("GB");
    final Country gb2 = Country.get("GB");

    assertEquals(true, us1.equals(us1));
    assertEquals(true, us1.equals(us2));
    assertEquals(true, gb1.equals(gb1));
    assertEquals(true, gb1.equals(gb2));
    assertEquals(false, us1.equals(null));
    assertEquals(false, us1.equals(gb1));
    assertEquals(false, us1.equals("str"));
  }

  @Test
  public void testToString() {
    final Country us = Country.get("US");
    System.out.println(us.toString());

    final Country gb = Country.get("GB");
    System.out.println(gb.toString());
  }
}

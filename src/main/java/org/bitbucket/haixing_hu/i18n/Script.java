/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.i18n;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.bitbucket.haixing_hu.io.IoUtils;
import org.bitbucket.haixing_hu.lang.Equality;
import org.bitbucket.haixing_hu.lang.Hash;
import org.bitbucket.haixing_hu.text.tostring.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * The class represents a script.
 *
 * @author Haixing Hu
 * @see <a href='http://cldr.unicode.org'>CLDR - Unicode Common Locale Data Repository</a>
 */
public final class Script {

  private static final String DATA_FILE = "/script/Script.properties";
  private static final String MESSAGE_BASENAME = "script/Script";

  private static final Logger LOGGER = LoggerFactory.getLogger(Script.class);
  private static final Map<String, Script> SCRIPTS = loadScripts();
  private static final MessageSource MESSAGE_SOURCE = loadMessageSource();

  private static Map<String, Script> loadScripts() {
    final Map<String, Script> scripts = new HashMap<>();
    final URL url = Script.class.getResource(DATA_FILE);
    if (url == null) {
      LOGGER.error("Failed to load script data from {}.", DATA_FILE);
      return scripts;
    }
    InputStream in = null;
    try {
      in = url.openStream();
      final Reader reader = new InputStreamReader(in, "UTF-8");
      final Properties props = new Properties();
      props.load(reader);
      for (final String code : props.stringPropertyNames()) {
        final String name = props.getProperty(code);
        final Script script = new Script(code, name);
        LOGGER.debug("Register the script {}: {}", code, name);
        scripts.put(code, script);
      }
    } catch (final IOException e) {
      LOGGER.error("Failed to load script data from {}.", DATA_FILE, e);
    } finally {
      IoUtils.closeQuietly(in);
    }
    return scripts;
  }

  private static MessageSource loadMessageSource() {
    final ResourceBundleMessageSource ms = new ResourceBundleMessageSource();
    ms.setDefaultEncoding("UTF-8");
    ms.setBasename(MESSAGE_BASENAME);
    return ms;
  }

  /**
   * Gets the collection of all scripts.
   *
   * @return the collection of all scripts.
   */
  public static Collection<Script> getAll() {
    return SCRIPTS.values();
  }

  /**
   * Gets the script with the specified code.
   *
   * @param code
   *          the code of a script.
   * @return the script with the specified code, or {@code null} if no such
   *         script.
   */
  public static Script get(final String code) {
    return SCRIPTS.get(code);
  }

  private final String code;
  private final String name;

  /**
   * Constructs a script.
   *
   * @param code
   *          the code of the script.
   * @param name
   *          the name of the script.
   */
  private Script(final String code, final String name) {
    this.code = code;
    this.name = name;
  }

  /**
   * Gets the code of this script.
   *
   * @return the code of this script.
   */
  public String getCode() {
    return code;
  }

  /**
   * Gets the name of this script.
   *
   * @return the name of this script.
   */
  public String getName() {
    return name;
  }

  /**
   * Gets the localized display name of this script.
   *
   * @param locale
   *          a locale.
   * @return the display name of this script in the specified locale.
   */
  public String getDisplayName(final Locale locale) {
    try {
      return MESSAGE_SOURCE.getMessage(code, null, locale);
    } catch (final NoSuchMessageException e) {
      LOGGER.error("No display name for the script '{}' in the locale {}.",
          code, locale);
      return name;
    }
  }

  @Override
  public int hashCode() {
    final int multiplier = 7;
    int result = 3;
    result = Hash.combine(result, multiplier, code);
    result = Hash.combine(result, multiplier, name);
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    final Script rhs = (Script) obj;
    return Equality.equals(code, rhs.code)
        && Equality.equals(name, rhs.name);
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
              .append("code", code)
              .append("name", name)
              .toString();
  }
}

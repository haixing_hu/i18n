/*
 * Copyright (c) 2014  Haixing Hu
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.bitbucket.haixing_hu.i18n;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.bitbucket.haixing_hu.io.IoUtils;
import org.bitbucket.haixing_hu.lang.Equality;
import org.bitbucket.haixing_hu.lang.Hash;
import org.bitbucket.haixing_hu.text.tostring.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;

/**
 * The class represents a language.
 *
 * @author Haixing Hu
 * @see <a href='http://cldr.unicode.org'>CLDR - Unicode Common Locale Data Repository</a>
 */
public final class Language {

  private static final String DATA_FILE = "/language/Language.properties";
  private static final String MESSAGE_BASENAME = "language/Language";

  private static final Logger LOGGER = LoggerFactory.getLogger(Language.class);
  private static final Map<String, Language> LANGUAGES = loadLanguages();
  private static final MessageSource MESSAGE_SOURCE = loadMessageSource();

  private static Map<String, Language> loadLanguages() {
    final Map<String, Language> languages = new HashMap<>();
    final URL url = Language.class.getResource(DATA_FILE);
    if (url == null) {
      LOGGER.error("Failed to load language data from {}.", DATA_FILE);
      return languages;
    }
    InputStream in = null;
    try {
      in = url.openStream();
      final Reader reader = new InputStreamReader(in, "UTF-8");
      final Properties props = new Properties();
      props.load(reader);
      for (final String code : props.stringPropertyNames()) {
        final String name = props.getProperty(code);
        final Language language = new Language(code, name);
        LOGGER.debug("Register the language {}: {}", code, name);
        languages.put(code, language);
      }
    } catch (final IOException e) {
      LOGGER.error("Failed to load language data from {}.", DATA_FILE, e);
    } finally {
      IoUtils.closeQuietly(in);
    }
    return languages;
  }

  private static MessageSource loadMessageSource() {
    final ResourceBundleMessageSource ms = new ResourceBundleMessageSource();
    ms.setDefaultEncoding("UTF-8");
    ms.setBasename(MESSAGE_BASENAME);
    return ms;
  }

  /**
   * Gets the collection of all languages.
   *
   * @return the collection of all languages.
   */
  public static Collection<Language> getAll() {
    return LANGUAGES.values();
  }

  /**
   * Gets the language with the specified code.
   *
   * @param code
   *          the code of a language.
   * @return the language with the specified code, or {@code null} if no such
   *         language.
   */
  public static Language get(final String code) {
    return LANGUAGES.get(code);
  }

  private final String code;
  private final String name;

  /**
   * Constructs a language.
   *
   * @param code
   *          the code of the language.
   * @param name
   *          the name of the language.
   */
  private Language(final String code, final String name) {
    this.code = code;
    this.name = name;
  }

  /**
   * Gets the code of this language.
   *
   * @return the code of this language.
   */
  public String getCode() {
    return code;
  }

  /**
   * Gets the name of this language.
   *
   * @return the name of this language.
   */
  public String getName() {
    return name;
  }

  /**
   * Gets the localized display name of this language.
   *
   * @param locale
   *          a locale.
   * @return the display name of this language in the specified locale.
   */
  public String getDisplayName(final Locale locale) {
    try {
      return MESSAGE_SOURCE.getMessage(code, null, locale);
    } catch (final NoSuchMessageException e) {
      LOGGER.error("No display name for the language '{}' in the locale {}.",
          code, locale);
      return name;
    }
  }

  @Override
  public int hashCode() {
    final int multiplier = 7;
    int result = 3;
    result = Hash.combine(result, multiplier, code);
    result = Hash.combine(result, multiplier, name);
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (obj.getClass() != getClass()) {
      return false;
    }
    final Language rhs = (Language) obj;
    return Equality.equals(code, rhs.code)
        && Equality.equals(name, rhs.name);
  }

  @Override
  public String toString() {
    return new ToStringBuilder(this)
              .append("code", code)
              .append("name", name)
              .toString();
  }
}
